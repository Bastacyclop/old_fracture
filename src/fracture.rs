#![license = "GPLv3"]
#![feature(phase, macro_rules)]

extern crate libc;
extern crate "num" as extnum;
extern crate getopts;
extern crate collections;

extern crate rsfml;

use std::num;
pub use extnum::complex::{Complex, Complex64};
use getopts::{optflag, Matches};

pub use rsfml::graphics as sfg;
pub use rsfml::window as sfw;
pub use rsfml::system as sfs;
pub use rsfml::traits as sft;

use sfg::RenderTarget;

use escape_time as et;
use tools::{Size};
//use interface::Cli;
//use interface::Command;

#[macro_escape] pub mod tools;
pub mod escape_time;
//pub mod interface;

fn main() {
    /*println!("Fracture CLI Test");

    let mut hello_opts = Vec::new();
    hello_opts.push(optflag("w", "world", "speak to the world"));

    let hello_cmd = Command {
        exec: hello,
        desc: [
            "Say hello in a useless way".to_string(),
            "This is quite useless so I wanted to tell you some useless things about it. What ? Still reading that ?".to_string()
        ],
        opts: hello_opts,
    };

    let mut cli = Cli::new();
    cli.add_cmd("hello".to_string(), hello_cmd);

    cli.run();
*/
    println!("Fracture Raw Test");

    let plug_path = Path::new("plugins");
    let seeds_path = plug_path.join("escape-time/seeds");
    let dyers_path = plug_path.join("escape-time/dyers");
    println!("Loading plugins from '{}':", plug_path.display());

    let seed_name = "mandelbrot.pin";
    let dyer_name = "simple.pin";
    println!(" - loading seed '{}' ...", seed_name);
    let seed = et::SeedSlot::load_from_file(&seeds_path.join(seed_name));
    println!(" - loading dyer '{}' ...", dyer_name);
    let dyer = et::DyerSlot::load_from_file(&dyers_path.join(dyer_name));
    println!("Plugins loaded.");

    let mut param = et::Parameters {
        inf: Complex { re:-1.8 , im:-1.2 },
        sup: Complex { re:0.8 , im:1.2 },
        size: Size { w:0.0 , h:0.0 },
        zoom: 300.0,
        iterations: 26.0,
    };
    param.guess_size();

    let size = Size {
        w: num::from_f64(param.size.w).expect("Wrong width parameter"),
        h: num::from_f64(param.size.h).expect("Wrong height parameter")
    };

    let mut render = et::Render::new(
        et::Generator {
            param: param,
            seed: seed,
            dyer: dyer
        }
        );
    render.run(4);

    let mut window = sfg::RenderWindow::new(
        sfw::VideoMode::new_init(size.w, size.h, 32),
        "Render - Fracture", sfw::DefaultStyle,
        &sfw::ContextSettings::default()
        ).expect("Cannot create a new Render Window");

    let mut clock = sfs::Clock::new();

    while window.is_open() {
        render.update();

        if clock.get_elapsed_time() > sfs::Time::with_milliseconds(50) {
            loop {
                match window.poll_event() {
                    sfw::event::Closed => window.close(),
                    sfw::event::NoEvent => break,
                    e => render.handle_event(e)
                }
            }

            window.clear(&sfg::Color::new_RGB(255, 255, 255));
                window.draw(&render);
            window.display();

            clock.restart();
        }
    }
}
/*
fn hello(matches: &Matches) {
    if matches.opt_present("world") {
        println!("Hello World !");
    } else {
        println!("Hello !");
    }
}
*/
