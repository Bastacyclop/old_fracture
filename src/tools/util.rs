use std::num::{mod, Num};
use super::{Rect, Point, Size};

pub fn divide_rect<T>(rect: &Rect<T>, n: uint) -> Vec<Rect<T>>
    where T: Clone+Num+PartialOrd+FromPrimitive {

    let mut pieces = Vec::new();
    if n == 0 {
        pieces.push((*rect).clone())
    } else {
        divide_rect_internal(rect, n, &mut pieces);
    }
    pieces
}

fn divide_rect_internal<T>(rect: &Rect<T>, n: uint, pieces: &mut Vec<Rect<T>>)
    where T: Clone+Num+PartialOrd+FromPrimitive {

    let p1_origin = rect.o.clone();
    let p2_origin;
    let p1_size;
    let p2_size;

    if rect.is_wide() {
        let half_w = rect.s.w.clone() / num::from_uint(2).expect("Can't divide by 2");

        p2_origin = Point { x: rect.o.x.clone() + half_w.clone(), y: rect.o.y.clone() };

        p1_size = Size { w: half_w.clone(),            h: rect.s.h.clone() };
        p2_size = Size { w: rect.s.w.clone() - half_w, h: rect.s.h.clone() };
    } else {
        let half_h = rect.s.h.clone() / num::from_uint(2).expect("Can't divide by 2");

        p2_origin = Point { x: rect.o.x.clone(), y: rect.o.y.clone() + half_h.clone() };

        p1_size = Size { w: rect.s.w.clone(), h: half_h.clone()    };
        p2_size = Size { w: rect.s.w.clone(), h: rect.s.h.clone() - half_h };
    };

    let p1 = Rect { o: p1_origin, s: p1_size };
    let p2 = Rect { o: p2_origin, s: p2_size };

    if n == 1 {
        pieces.push(p1);
        pieces.push(p2);
    }
    else {
        divide_rect_internal(&p1, n - 1, pieces);
        divide_rect_internal(&p2, n - 1, pieces);
    }
}

#[test]
fn test_divide_rect_by_zero() {
    let rect = Rect {
        o: Point { x: 5u, y: 5 },
        s: Size { w: 10, h: 10 }
    };

    let mut pieces = divide_rect(&rect, 0);

    assert!(rect == pieces.pop().unwrap());
    assert!(pieces.is_empty());
}

#[test]
fn test_divide_rect() {
    let rect = Rect{
        o: Point {x:100u, y:100},
        s: Size {w:200, h:200}
    };

    let pieces = divide_rect(&rect, 2);

    let expected_pieces = [
        Rect {
            o: Point {x:100, y:100},
            s: Size {w:100, h:100}
        },
        Rect {
            o: Point {x:200, y:100},
            s: Size {w:100, h:100}
        },
        Rect {
            o: Point {x:100, y:200},
            s: Size {w:100, h:100}
            },
        Rect {
            o: Point {x:200, y:200},
            s: Size {w:100, h:100}
            },
    ];

    assert!(pieces.as_slice() == expected_pieces);
}
