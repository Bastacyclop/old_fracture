use std::mem;
use std::dynamic_lib::DynamicLibrary;

macro_rules! load_plugin_symbol(
    ($name:expr from $lib:expr) => (
        mem::transmute(plugin::load_symbol_from_library($name, $lib))
    )
);

pub unsafe fn load_symbol_from_library(name: &str, lib: &DynamicLibrary) -> *mut int {
    match lib.symbol::<int>(name) {
        Ok(raw_ptr) => raw_ptr,
        Err(err) => panic!("Can't load plugin's '{}' symbol: {}", name, err)
    }
}

pub fn load_library(path: &Path) -> DynamicLibrary {
    match DynamicLibrary::open(Some(path)) {
        Ok(lib) => lib,
        Err(err) => panic!("Can't load plugin: {}", err)
    }
}
