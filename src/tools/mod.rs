pub use self::structs::{Point, Size, Rect, Block, Color};
pub use self::util::divide_rect;

#[macro_escape] pub mod plugin;
pub mod structs;
pub mod util;
