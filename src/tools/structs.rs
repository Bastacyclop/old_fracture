use std::num;

#[deriving(PartialEq, Eq, Clone)]
pub struct Point<T> {
    pub x: T,
    pub y: T
}

#[deriving(PartialEq, Eq, Clone)]
pub struct Size<T> {
    pub w: T,
    pub h: T
}

#[deriving(PartialEq, Eq, Clone)]
pub struct Rect<T> {
    pub o: Point<T>,
    pub s: Size<T>
}

#[deriving(PartialEq, Eq, Clone)]
pub struct Block<T> {
    pub beg: T,
    pub end: T
}

#[deriving(PartialEq, Eq, Clone)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8
}

impl<T> Point<T> where T: Add<T, T>+Copy {
    pub fn plus(&self, other: &Point<T>) -> Point<T> {
        Point {
            x: self.x + other.x,
            y: self.y + other.y
        }
    }

    pub fn add(&mut self, other: &Point<T>) {
        self.x = self.x + other.x;
        self.y = self.y + other.y;
    }
}

impl<T> Rect<T> where T: PartialOrd {
    pub fn is_high(&self) -> bool {
        self.s.h > self.s.w
    }

    pub fn is_wide(&self) -> bool {
        self.s.w > self.s.h
    }
}
