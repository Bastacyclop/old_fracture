use getopts::{getopts, usage, OptGroup, Matches};

pub struct Command {
    pub exec: fn (&Matches),
    pub desc: [String, ..2],
    pub opts: Vec<OptGroup>,
}

impl Command {
    pub fn new(callback: fn (&Matches)) -> Command {
        Command {
            opts: Vec::new(),
            exec: callback,
            desc: ["No description".to_string(), "".to_string()]
        }
    }

    pub fn execute(&mut self, args: &[String]) {
        let matches = match getopts(args, self.opts.as_slice()) {
            Ok(m) => m,
            Err(err) => {
                println!("{}", err.to_err_msg());
                return
            }
        };
        (self.exec)(&matches);
    }

    pub fn usage(&self) -> String {
        usage(format!("{}\n{}", self.desc[0], self.desc[1]).as_slice(), self.opts.as_slice())
    }
}
