use std::io::stdin;
use std::io::stdio::flush;
use std::collections::hash_map::{HashMap, Entry};

use super::Command;

pub struct Cli {
    cmds: HashMap<String, Command>
}

impl Cli {
    pub fn new() -> Cli {
        Cli { cmds: HashMap::new() }
    }

    pub fn add_cmd(&mut self, name: String, cmd: Command) {
        match self.cmds.entry(name) {
            Entry::Vacant(entry) => { entry.set(cmd); }
            _ => {}
        }
    }

    pub fn run(&mut self) {
        let mut input = stdin();

        loop {
            print!("> ");
            flush();

            let line = match input.read_line() {
                Ok(ln) => ln,
                Err(err) => panic!("Cannot read input line: {}", err)
            };

            let words: Vec<String> = line.as_slice().words().map(|x| x.to_string()).collect();

            if words.len() == 0 {
                continue;
            }

            let ref cmd_name = words[0];
            match cmd_name.as_slice() {
                "help" => self.help(words.tail()),
                "exit"|"quit"|"q" => {
                        println!("Good bye !");
                        break;
                    },
                _ => {
                        let mut found = false;
                        for (name, cmd) in self.cmds.iter_mut() {
                            if name.as_slice() == cmd_name.as_slice() {
                                cmd.execute(words.tail());
                                found = true;
                            }
                        }
                        if !found {
                            println!("This is not a command (try 'help')");
                        }
                    }
            }
        }
    }

    pub fn help(&self, args: &[String]) {
        if args.len() == 0 {
            self.global_help();
        } else {
            for (name, cmd) in self.cmds.iter() {
                if name == &args[0] {
                    println!("{}", cmd.usage());
                    return;
                }
            }
            self.global_help();
        }
    }

    fn global_help(&self) {
        println!("This is a Command Line Interface");
        println!("Commands:");
        for (name, cmd) in self.cmds.iter() {
            println!("\t- {}\t\t{}", name, cmd.desc[0]);
        }
    }
}
