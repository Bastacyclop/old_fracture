use libc;
use std::sync::Arc;
use std::mem;
use std::ptr;
use std::dynamic_lib::DynamicLibrary;

use tools::{Point, Color};
use tools::plugin;
use super::Parameters;

type DyerData = *mut libc::c_void;

type InitCallback = extern "C" fn (*const Parameters) -> DyerData;
type DyeCallback  = extern "C" fn (DyerData, *const Point<uint>, uint) -> Color;
type DropCallback = extern "C" fn (DyerData);

pub struct DyerSlot {
    lib: Arc<DynamicLibrary>,
    data: DyerData,
    call_init: InitCallback,
    call_dye:  DyeCallback,
    call_drop: DropCallback,
}

impl DyerSlot {
    pub fn load_from_file(path: &Path) -> DyerSlot {
        let lib = plugin::load_library(path);

        let call_init = unsafe { load_plugin_symbol!("init" from &lib) };
        let call_dye  = unsafe { load_plugin_symbol!("dye"  from &lib) };
        let call_drop = unsafe { load_plugin_symbol!("drop" from &lib) };

        DyerSlot {
            lib: Arc::new(lib),
            data: ptr::null_mut(),
            call_init: call_init,
            call_dye:  call_dye,
            call_drop: call_drop,
        }
    }

    pub fn init_or_reset(&mut self, param: Parameters) {
        if self.data.is_not_null() {
            (self.call_drop)(self.data)
        }
        self.data = (self.call_init)(&param);
    }

    pub fn dye(&mut self, point: Point<uint>, iter: uint) -> Color {
        (self.call_dye)(self.data, &point, iter)
    }
}

impl Drop for DyerSlot {
    fn drop(&mut self) {
        if !self.data.is_null() { (self.call_drop)(self.data) }
    }
}

impl Clone for DyerSlot {
    fn clone(&self) -> DyerSlot {
        DyerSlot {
            lib: self.lib.clone(),
            data: ptr::null_mut(),
            call_init: self.call_init,
            call_dye:  self.call_dye,
            call_drop: self.call_drop
        }
    }
}

unsafe impl Send for DyerSlot {}
