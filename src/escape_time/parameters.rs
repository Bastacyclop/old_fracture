use {Complex64, Complex};

use tools::{Size, Rect};

#[deriving(Clone)]
pub struct Parameters {
    pub inf: Complex64,
    pub sup: Complex64,
    pub size: Size<f64>,
    pub zoom: f64,
    pub iterations: f64,
}

impl Parameters {
    pub fn default() -> Parameters {
        Parameters {
            inf: Complex { re:0.0, im:0.0 },
            sup: Complex { re:0.0, im:0.0 },
            size: Size { w:0.0, h:0.0 },
            zoom: 100.0,
            iterations: 12.0
        }
    }

    pub fn guess_size(&mut self) {
        self.size.w = (self.sup.re-self.inf.re) * self.zoom;
        self.size.h = (self.sup.im-self.inf.im) * self.zoom;
    }

    pub fn guess_zoom(&mut self) {
        self.zoom = self.size.w / (self.sup.re-self.inf.re);
    }

    pub fn guess_sup(&mut self) {
        self.sup.re = self.size.w / self.zoom + self.inf.re;
        self.sup.im = self.size.h / self.zoom + self.inf.im;
    }

    pub fn guess_inf(&mut self) {
        self.inf.re = self.inf.re - self.size.w / self.zoom;
        self.inf.im = self.inf.im - self.size.h / self.zoom;
    }

    pub fn guess_with_center(&mut self, center: Complex64) {
        let half_size_re = self.size.w * self.zoom / 2.;
        let half_size_im = self.size.h * self.zoom / 2.;

        self.inf.re = center.re - half_size_re;
        self.sup.re = center.re + half_size_re;
        self.inf.im = center.im - half_size_im;
        self.sup.im = center.im + half_size_im;
    }

    pub fn clone_rect(&self, r: Rect<f64>) -> Parameters {
        let mut a = (*self).clone();
        a.inf.re = r.o.x;
        a.inf.im = r.o.y;
        a.sup.re = r.o.x + r.s.w;
        a.sup.im = r.o.y + r.s.h;
        a.guess_size();
        a
    }
}
