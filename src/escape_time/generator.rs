use std::num;

use tools::{Point, Size, Color};
use super::{Parameters, SeedSlot, DyerSlot};

pub struct Generator {
    pub param: Parameters,
    pub seed: SeedSlot,
    pub dyer: DyerSlot,
}

impl Generator {
    pub fn render_whole(&mut self,
                        has_progressed: |uint|,
                        point_rendered: |Point<uint>, Color|) {
        let size: Size<uint> = Size {
            w: num::from_f64(self.param.size.w).expect("Wrong width parameter"),
            h: num::from_f64(self.param.size.h).expect("Wrong height parameter")
        };

        self.seed.init_or_reset(self.param.clone());
        self.dyer.init_or_reset(self.param.clone());

        let mut point = Point { x:0, y:0 };
        let mut progress = 0;

        loop {
            if size.h*point.x + point.y+1 > size.w*size.h*progress/100 {
                has_progressed(progress);
                progress += 1;
            }

            point_rendered(point.clone(), self.render_point(point.clone()));

            if point.y == size.h-1 {
                if point.x == size.w-1 {
                    has_progressed(progress);
                    break;
                }

                point.y = 0;
                point.x += 1;
            }
            else {
                point.y += 1;
            }
        }
    }

    fn render_point(&mut self, point: Point<uint>) -> Color {
        self.seed.plant_at(point.clone());

        let mut i = 1;
        let max_i: uint = num::from_f64(self.param.iterations).expect("Wrong iterations parameter");
        loop {
            self.seed.grow();
            if i == max_i || self.seed.is_ripe() {
                break;
            }
            i += 1;
        }

        self.dyer.dye(point, i)
    }
}
