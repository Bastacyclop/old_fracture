use std::num;
use std::rc::Rc;
use std::cell::RefCell;
use {Complex64, Complex};

use sfs;
use sfw;
use sfg;
use sft;
use sfw::mouse::MouseButton;

use tools::{mod, Size, Point, Rect};
use super::Generator;

pub type Pixel = (Point<uint>, [u8, ..4]);

pub struct Render {
    generator: Generator,
    sender: Sender<Pixel>,
    receiver: Receiver<Pixel>,

    image_pos: sfg::rc::Text,
    complex_pos: sfg::rc::Text,

    selection: sfg::VertexArray,
    selecting: bool,

    sprite: sfg::rc::Sprite,
    texture: Rc<RefCell<sfg::Texture>>,
}

impl sft::Drawable for Render {
    fn draw<RT: sfg::RenderTarget>(&self, target: &mut RT) {
        target.draw(&self.sprite);
        target.draw(&self.image_pos);
        target.draw(&self.complex_pos);
        if self.selecting {
            target.draw(&self.selection);
        }
    }
}

impl Render {
    pub fn new(generator: Generator) -> Render {
        let size = Size {
            w: num::from_f64(generator.param.size.w).expect("Wrong width parameter"),
            h: num::from_f64(generator.param.size.h).expect("Wrong height parameter"),
        };

        let texture = Rc::new(RefCell::new(sfg::Texture::new(size.w, size.h).unwrap()));

        let font = Rc::new(RefCell::new(
            sfg::Font::new_from_file("resources/fonts/FiraMonoOT-Regular.otf").unwrap()
            ));

        let (sender, receiver) = channel::<Pixel>();

        let mut r = Render {
            generator: generator,
            sender: sender,
            receiver: receiver,
            sprite: sfg::rc::Sprite::new_with_texture(texture.clone()).unwrap(),
            texture: texture,
            image_pos: sfg::rc::Text::new_init("", font.clone(), 10).unwrap(),
            complex_pos: sfg::rc::Text::new_init("", font, 10).unwrap(),
            selection: sfg::VertexArray::new_init(sfg::LinesStrip, 5).unwrap(),
            selecting: false,
        };

        //r.window.set_mouse_cursor_visible(false);

        //let w = num::from_int::<f32>(size.w).unwrap();
        let h = num::from_uint::<f32>(size.h).unwrap();

        r.image_pos.set_color(&sfg::Color::new_RGB(255, 255, 255));
        r.image_pos.set_position2f(10., h-40.);
        r.complex_pos.set_color(&sfg::Color::new_RGB(255, 55, 55));
        r.complex_pos.set_position2f(80., h-40.);

        r
    }

    pub fn run(&mut self, divisions: uint) {
        let rects = tools::divide_rect(
            &Rect {
                o: Point {
                    x: self.generator.param.inf.re,
                    y: self.generator.param.inf.im,
                },
                s: Size {
                    w: self.generator.param.sup.re - self.generator.param.inf.re,
                    h: self.generator.param.sup.im - self.generator.param.inf.im,
                },
            }, divisions);

        for r in rects.iter() {
            let p = self.generator.param.clone_rect(r.clone());
            let s = self.generator.seed.clone();
            let d = self.generator.dyer.clone();

            let sender = self.sender.clone();
            let o = Point {
                x: num::from_f64::<uint>(
                       (p.inf.re - self.generator.param.inf.re) * self.generator.param.zoom
                       ).expect("Wrong origin x"),
                y: num::from_f64::<uint>(
                       (p.inf.im - self.generator.param.inf.im) * self.generator.param.zoom
                       ).expect("Wrong origin y"),
            };

            spawn(move || {
                let mut generator = Generator {
                    param: p,
                    seed: s,
                    dyer: d,
                };

                generator.render_whole(
                    |_x| {},
                    |point, color| { sender.send((o.plus(&point), [color.r, color.g, color.b, 255])); }
                    );
            });
        }
    }

    pub fn update(&mut self) {
        match self.receiver.try_recv() {
            Ok((point, color)) => {
                self.texture.borrow_mut().update_from_pixels(&color, 1, 1, point.x, point.y)
            }
            _ => {}
        }
    }

    pub fn handle_event(&mut self, event: sfw::event::Event) {
        match event {
            sfw::event::MouseWheelMoved {delta, x, y} => {
                let factor = num::from_i32::<f32>(delta).unwrap()/50. + self.sprite.get_scale().x;
                self.sprite.set_scale2f(factor, factor);
                self.update_pos(x as int, y as int);
            }
            sfw::event::MouseButtonPressed {button, x, y} => {
                match button {
                    MouseButton::MouseLeft => {self.start_select(x as int, y as int)}
                    MouseButton::MouseRight => {}
                    MouseButton::MouseMiddle => {}
                    _ => {}
                }
                self.update_pos(x as int, y as int);
            }
            sfw::event::MouseButtonReleased {button, x, y} => {
                match button {
                    MouseButton::MouseLeft => {self.end_select(x as int, y as int)}
                    MouseButton::MouseRight => {}
                    MouseButton::MouseMiddle => {}
                    _ => {}
                }
                self.update_pos(x as int, y as int);
            }
            sfw::event::MouseMoved {x, y} => {
                self.update_pos(x as int, y as int);
            }
            _ => {}
        }
    }

    fn zoom(&mut self, ratio: f64, center: Complex64) {
        self.generator.param.zoom = self.generator.param.zoom * ratio;
        self.generator.param.guess_with_center(center);
    }

    fn get_complex_pos(&self, x: int, y: int) -> Complex64 {
        let x = num::from_int::<f64>(x).unwrap();
        let y = num::from_int::<f64>(y).unwrap();
        Complex {
            re: self.generator.param.inf.re + x/self.generator.param.zoom,
            im: self.generator.param.inf.im + y/self.generator.param.zoom,
        }
    }

    fn update_pos(&mut self, x: int, y: int) {
        let c = self.get_complex_pos(x, y);
        let x = num::from_int::<f32>(x).unwrap();
        let y = num::from_int::<f32>(y).unwrap();

        self.image_pos.set_string(format!("x: {}\ny: {}", x, y).as_slice());

        self.complex_pos.set_string(format!("re: {}\nim: {}", c.re, c.im).as_slice());

        if self.selecting {
            self.selection.get_vertex(1).position =
                sfs::Vector2f::new(self.selection[0].position.x, y);
            self.selection.get_vertex(2).position =
                sfs::Vector2f::new(x, y);
            self.selection.get_vertex(3).position =
                sfs::Vector2f::new(x, self.selection[0].position.y);
        }
    }

    fn start_select(&mut self, x: int, y: int) {
        let pos = sfs::Vector2f::new(
            num::from_int::<f32>(x).unwrap(),
            num::from_int::<f32>(y).unwrap()
            );

        self.selection.get_vertex(0).position = pos;
        self.selection.get_vertex(4).position = pos;

        self.selecting = true;
    }

    fn end_select(&mut self, x: int, y: int) {
        self.selecting = false;
    }
}
