use libc;
use std::sync::Arc;
use std::ptr;
use std::mem;
use std::dynamic_lib::DynamicLibrary;

use tools::Point;
use tools::plugin;
use super::Parameters;

type SeedData = *mut libc::c_void;

type InitCallback    = extern "C" fn (*const Parameters) -> SeedData;
type PlantAtCallback = extern "C" fn (SeedData, *const Point<uint>);
type GrowCallback    = extern "C" fn (SeedData);
type IsRipeCallback  = extern "C" fn (SeedData) -> bool;
type DropCallback    = extern "C" fn (SeedData);

pub struct SeedSlot {
    lib: Arc<DynamicLibrary>,
    data: SeedData,
    call_init:      InitCallback,
    call_plant_at:  PlantAtCallback,
    call_grow:      GrowCallback,
    call_is_ripe:   IsRipeCallback,
    call_drop:      DropCallback,
}

impl SeedSlot {
    pub fn load_from_file(path: &Path) -> SeedSlot {
        let lib = plugin::load_library(path);

        let call_init     = unsafe { load_plugin_symbol!("init"     from &lib) };
        let call_plant_at = unsafe { load_plugin_symbol!("plant_at" from &lib) };
        let call_grow     = unsafe { load_plugin_symbol!("grow"     from &lib) };
        let call_is_ripe  = unsafe { load_plugin_symbol!("is_ripe"  from &lib) };
        let call_drop     = unsafe { load_plugin_symbol!("drop"     from &lib) };

        SeedSlot {
            lib: Arc::new(lib),
            data: ptr::null_mut(),
            call_init:     call_init,
            call_plant_at: call_plant_at,
            call_grow:     call_grow,
            call_is_ripe:  call_is_ripe,
            call_drop:     call_drop,
        }
    }

    pub fn init_or_reset(&mut self, param: Parameters) {
        if self.data.is_not_null() {
            (self.call_drop)(self.data)
        }
        self.data = (self.call_init)(&param);
    }

    pub fn plant_at(&mut self, p: Point<uint>) {
        (self.call_plant_at)(self.data, &p);
    }

    pub fn grow(&mut self) {
        (self.call_grow)(self.data);
    }

    pub fn is_ripe(&mut self) -> bool {
        (self.call_is_ripe)(self.data)
    }
}

impl Drop for SeedSlot {
    fn drop(&mut self) {
        if !self.data.is_null() { (self.call_drop)(self.data) }
    }
}

impl Clone for SeedSlot {
    fn clone(&self) -> SeedSlot {
        SeedSlot {
            lib: self.lib.clone(),
            data: ptr::null_mut(),
            call_init:      self.call_init,
            call_plant_at:  self.call_plant_at,
            call_grow:      self.call_grow,
            call_is_ripe:   self.call_is_ripe,
            call_drop:      self.call_drop,
        }
    }
}

unsafe impl Send for SeedSlot {}
