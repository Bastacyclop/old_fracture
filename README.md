## Fracture

Fracture is a fractal generator.

## Installation

0. Install rust-sfml dependencies.
0. Use `$ ./build_plugins.sh` to build the plugins
0. Simply use cargo.

## TODO

- [ ] Clean the code
- [ ] Optimizations
    - [ ] Symmetry ?
- [ ] Make an interface and rework `Render`
- [ ] Rework `Parameter` guess
- [ ] Secure slots ?
- [ ] Add more generation methods

### Authors

- Thomas Koehler - main developer <basta.t.k+git@gmail.com>

### Fracture is using :

- SFML <http://www.sfml-dev.org/> with rust-sfml <http://www.rust-sfml.org/>
