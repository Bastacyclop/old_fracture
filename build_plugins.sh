echo '<----------------- Building Plugins --------------->'
for in_path in `find plugins/ -name *.c`;
do
    out_path=${in_path%\.c}.pin
    echo " - building '$out_path' ..."
    gcc -std=c99 -shared -fPIC $in_path -o $out_path
done
echo '>----------------------- End ----------------------<'

