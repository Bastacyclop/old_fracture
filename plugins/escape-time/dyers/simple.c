#include "../et.h"

Color* init(Parameters const *const param) {
    r_uint max_iter = (r_uint)(param->iterations);

    Color* colors = malloc(sizeof(Color)*(max_iter+1));
    if (colors == 0) exit(1);

    for(r_uint i = 0; i <= max_iter; ++i) {
        r_f64 r = (r_f64)(i)/param->iterations;

        colors[i].r = 55 *r;
        colors[i].g = 255*r;
        colors[i].b = 55 *r;
    }

    return colors;
}

void drop(Color* colors) {
    free(colors);
}

Color dye(Color* colors, Size const *const point, r_uint const iter) {
    return colors[iter];
}
