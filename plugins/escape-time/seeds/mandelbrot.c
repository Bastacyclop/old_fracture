#include "../et.h"

typedef struct Seed {
    Parameters param;
    complex z;
    complex c;
} Seed;

Seed* init(Parameters const *const param) {
    Seed *seed = malloc(sizeof(Seed));
    if(seed == NULL) exit(1);

    seed->param = *param;
    seed->z = 0;
    seed->c = 0;

    return seed;
}

void drop(Seed *seed) {
    free(seed);
}

void plant_at(Seed *seed, Point const *const p) {
    seed->z = 0 + 0*I;
    seed->c = (p->x/seed->param.zoom + seed->param.inf.re)
            + (p->y/seed->param.zoom + seed->param.inf.im)*I;
}

void grow(Seed *seed) {
    seed->z = cpow(seed->z, 2) + seed->c;
}

bool is_ripe(Seed *seed) {
    return cabs(seed->z) > 2;
}
