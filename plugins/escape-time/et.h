#include "../plugins.h"

typedef struct Parameters {
    Complex inf;
    Complex sup;
    Sizef size;
    r_f64 zoom;
    r_f64 iterations;
} Parameters;
