#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <stdint.h>

typedef enum { false, true } bool;

typedef uintptr_t r_uint;
typedef double r_f64;
typedef unsigned char r_u8;

typedef struct Point {
    r_uint x;
    r_uint y;
} Point;

typedef struct Size {
    r_uint w;
    r_uint h;
} Size;

typedef struct Sizef {
    r_f64 w;
    r_f64 h;
} Sizef;

typedef struct Color {
    r_u8 r;
    r_u8 g;
    r_u8 b;
} Color;

typedef struct Complex {
    r_f64 re;
    r_f64 im;
} Complex;
